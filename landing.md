# Simple Phone (WIP)

Un repo para mi proyecto de telefono DIY simple,
hecho con una pantallita de papel electrónico,
un chip SIM800L, y un microcontrolador.

La documentacion esta en el sitio de gitlab pages, generado por gitbuilding con `gitbuilding generate ci`, y publicada en:

* https://naikymen.gitlab.io/simple_phone/

![cablerio de componentes](media/cablerio.jpg  "cablerio")

> Ahora son 4 capacitores de 220 uF. Pero creo que igual hace falta mejorar la fuente o poner más caps.

## Documentación

Algunas de las páginas con documentación son:

* [.](part01_sim800l.md){step}
* [.](part02_display.md){step}
* [.](part03_microcontroller.md){step}
* [.](part04_GPSmodule.md){step}
* [.](part06_power_supply.md){step}
* [.](part05_firmware_libraries.md){step}
* Link a la [lista de materiales]{BOM} del proyecto.
