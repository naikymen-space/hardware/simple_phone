#include <SoftwareSerial.h>

// setup gps serial
int gpsTxPin = 27;
int gpsRxPin = 26;
SoftwareSerial gpsSerial(gpsTxPin, gpsRxPin);

// Funciona!
// Referencia de codigos: 
// http://aprs.gids.nl/nmea/#gll
// https://github.com/SlashDevin/NeoGPS

void setup()
{
  Serial.begin(9600);  //set monitor to 9600
 
  gpsSerial.begin(9600); //adjust for GPS unit

  Serial.println("Ready!");
}

void loop()
{
  while(gpsSerial.available())
  {
    char c = gpsSerial.read();
    Serial.print(c);
  }
}
