/**************************************************************************
 This is an example for our Monochrome OLEDs based on SSD1306 drivers
 **************************************************************************/

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


#include <NMEAGPS.h>

//======================================================================
//  Program: NMEA.ino
//
//  Description:  This program uses the fix-oriented methods available() and
//    read() to handle complete fix structures.
//
//    When the last character of the LAST_SENTENCE_IN_INTERVAL (see NMEAGPS_cfg.h)
//    is decoded, a completed fix structure becomes available and is returned
//    from read().  The new fix is saved the 'fix' structure, and can be used
//    anywhere, at any time.
//
//    If no messages are enabled in NMEAGPS_cfg.h, or
//    no 'gps_fix' members are enabled in GPSfix_cfg.h, no information will be
//    parsed, copied or printed.
//
//  Prerequisites:
//     1) Your GPS device has been correctly powered.
//          Be careful when connecting 3.3V devices.
//     2) Your GPS device is correctly connected to an Arduino serial port.
//          See GPSport.h for the default connections.
//     3) You know the default baud rate of your GPS device.
//          If 9600 does not work, use NMEAdiagnostic.ino to
//          scan for the correct baud rate.
//     4) LAST_SENTENCE_IN_INTERVAL is defined to be the sentence that is
//          sent *last* in each update interval (usually once per second).
//          The default is NMEAGPS::NMEA_RMC (see NMEAGPS_cfg.h).  Other
//          programs may need to use the sentence identified by NMEAorder.ino.
//     5) NMEAGPS_RECOGNIZE_ALL is defined in NMEAGPS_cfg.h
//
//  'Serial' is for debug output to the Serial Monitor window.
//
//  License:
//    Copyright (C) 2014-2017, SlashDevin
//
//    This file is part of NeoGPS
//
//    NeoGPS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    NeoGPS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with NeoGPS.  If not, see <http://www.gnu.org/licenses/>.
//
//======================================================================

//-------------------------------------------------------------------------
//  The GPSport.h include file tries to choose a default serial port
//  for the GPS device.  If you know which serial port you want to use,
//  edit the GPSport.h file.

#include <GPSport.h>

//------------------------------------------------------------
// For the NeoGPS example programs, "Streamers" is common set
//   of printing and formatting routines for GPS data, in a
//   Comma-Separated Values text format (aka CSV).  The CSV
//   data will be printed to the "debug output device".
// If you don't need these formatters, simply delete this section.

#include <Streamers.h>

//------------------------------------------------------------
// This object parses received characters
//   into the gps.fix() data structure

static NMEAGPS  gps;

//------------------------------------------------------------
//  Define a set of GPS fix information.  It will
//  hold on to the various pieces as they are received from
//  an RMC sentence.  It can be used anywhere in your sketch.

static gps_fix  fix;

//--------------------------

/* 8 TIME ZONE STUFF */

static const int32_t          zone_hours   = -3L; // Argentina https://en.wikipedia.org/wiki/Coordinated_Universal_Time#/media/File:World_Time_Zones_Map.png
static const int32_t          zone_minutes =  0L; // usually zero
static const NeoGPS::clock_t  zone_offset  =
                                zone_hours   * NeoGPS::SECONDS_PER_HOUR +
                                zone_minutes * NeoGPS::SECONDS_PER_MINUTE;

static const uint8_t fallMonth =  3;
static const uint8_t fallDate  = 14; // latest 2nd Sunday
static const uint8_t fallHour  =  2;
static const uint8_t springMonth   = 11;
static const uint8_t springDate    =  7; // latest 1st Sunday
static const uint8_t springHour    =  2;
#define CALCULATE_DST

/**************************************************************
 *
 * TinyGSM Getting Started guide:
 *   https://tiny.cc/tinygsm-readme
 *
 * NOTE:
 * Some of the functions may be unavailable for your modem.
 * Just comment them out.
 *
 **************************************************************/

// Select your modem:
#define TINY_GSM_MODEM_SIM800

// Set serial for debug console (to the Serial Monitor, default speed 115200)
#define SerialMon Serial

// Set serial for AT commands (to the module)
// Use Hardware Software Serial on Uno, Nano
#include <SoftwareSerial.h>
//SoftwareSerial SerialAT(33, 32);  // ESP32: RX, TX
SoftwareSerial SerialAT(14, 12); //  ESP8226 12E: GPIO14 (D5) & GPIO12 (D6)

// See all AT commands, if wanted
// #define DUMP_AT_COMMANDS

// Define the serial console for debug prints, if needed
#define TINY_GSM_DEBUG SerialMon

// Your GPRS credentials, if any
const char apn[] = "YourAPN";
const char gprsUser[] = "";
const char gprsPass[] = "";

// Modem object
#include <TinyGsmClient.h>
TinyGsm        modem(SerialAT);

/* Old SIM Stuff */
//Create software serial object to communicate with SIM800L
//SoftwareSerial simSerial(33, 32); //SIM800L Tx & Rx is connected to Arduino #3 & #2


void setup(){
  Serial.begin(9600);
  
  // Display stuff
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }   
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE, BLACK);
  
  display.setCursor(0, 10);
  display.println("Hello GPS!");
  display.display();

  delay(1000);

  // New sim stuff
  SerialAT.begin(9600);
  delay(10);
  
  modem.setBaud(9600);

  String name = modem.getModemName();
  DBG("Modem Name:", name);

  String modemInfo = modem.getModemInfo();
  DBG("Modem Info:", modemInfo);

  
  Serial.println("DBG MSG 1");

  if (!modem.waitForNetwork(600000L, true)) {
    DBG("Waiting for network... (10 second delay)");
    display.setCursor(0, 20);
    display.println("Waiting for network...");
    display.display();
    
    delay(10000);
    return;
  }

  
  Serial.println("DBG MSG 2");

  if (modem.isNetworkConnected()){
    DBG("Network connected.");
    display.setCursor(0, 30);
    display.println("Network connected.");
    display.display();
    
  } else {
    DBG("Network connection timed out.");
    display.setCursor(0, 30);
    display.println("Network connection timed out.");
    display.display();
  }

  Serial.println("DBG MSG 3");

  //  DBG("Connecting to", apn);
  //  if (!modem.gprsConnect(apn, gprsUser, gprsPass)) {
  //    delay(10000);
  //    return;
  //  }
  //
  //  bool res = modem.isGprsConnected();
  //  DBG("GPRS status:", res ? "connected" : "not connected");
  //
  //  String ccid = modem.getSimCCID();
  //  DBG("CCID:", ccid);
  //
  //  String imei = modem.getIMEI();
  //  DBG("IMEI:", imei);
  //
  //  String imsi = modem.getIMSI();
  //  DBG("IMSI:", imsi);
  //
  //  String cop = modem.getOperator();
  //  DBG("Operator:", cop);
  //
  //  IPAddress local = modem.localIP();
  //  DBG("Local IP:", local);
  //
  //  int csq = modem.getSignalQuality();
  //  DBG("Signal quality:", csq);

  // Old SIM stuff
  //Begin serial communication with Arduino and SIM800L
  //  simSerial.begin(9600);
    
  //  simSerial.println("AT"); //Once the handshake test is successful, it will back to OK
  //  updateSerial();
  
  // GPS stuff
  DEBUG_PORT.begin(9600);
  while (!DEBUG_PORT)
    ;

  DEBUG_PORT.print( F("NMEA.INO: started\n") );
  DEBUG_PORT.print( F("  fix object size = ") );
  DEBUG_PORT.println( sizeof(gps.fix()) );
  DEBUG_PORT.print( F("  gps object size = ") );
  DEBUG_PORT.println( sizeof(gps) );
  DEBUG_PORT.println( F("Looking for GPS device on " GPS_PORT_NAME) );

  #ifndef NMEAGPS_RECOGNIZE_ALL
    #error You must define NMEAGPS_RECOGNIZE_ALL in NMEAGPS_cfg.h!
  #endif

  #ifdef NMEAGPS_INTERRUPT_PROCESSING
    #error You must *NOT* define NMEAGPS_INTERRUPT_PROCESSING in NMEAGPS_cfg.h!
  #endif

  #if !defined( NMEAGPS_PARSE_GGA ) & !defined( NMEAGPS_PARSE_GLL ) & \
      !defined( NMEAGPS_PARSE_GSA ) & !defined( NMEAGPS_PARSE_GSV ) & \
      !defined( NMEAGPS_PARSE_RMC ) & !defined( NMEAGPS_PARSE_VTG ) & \
      !defined( NMEAGPS_PARSE_ZDA ) & !defined( NMEAGPS_PARSE_GST )

    DEBUG_PORT.println( F("\nWARNING: No NMEA sentences are enabled: no fix data will be displayed.") );

  #else
    if (gps.merging == NMEAGPS::NO_MERGING) {
      DEBUG_PORT.print  ( F("\nWARNING: displaying data from ") );
      DEBUG_PORT.print  ( gps.string_for( LAST_SENTENCE_IN_INTERVAL ) );
      DEBUG_PORT.print  ( F(" sentences ONLY, and only if ") );
      DEBUG_PORT.print  ( gps.string_for( LAST_SENTENCE_IN_INTERVAL ) );
      DEBUG_PORT.println( F(" is enabled.\n"
                            "  Other sentences may be parsed, but their data will not be displayed.") );
    }
  #endif

  DEBUG_PORT.print  ( F("\nGPS quiet time is assumed to begin after a ") );
  DEBUG_PORT.print  ( gps.string_for( LAST_SENTENCE_IN_INTERVAL ) );
  DEBUG_PORT.println( F(" sentence is received.\n"
                        "  You should confirm this with NMEAorder.ino\n") );

  trace_header( DEBUG_PORT );
  DEBUG_PORT.flush();

  gpsPort.begin( 9600 );
}

//--------------------------

void loop(){
  //  This is the main GPS parsing loop.
  GPSloop();
}


//----------------------------------------------------------------
//  This function gets called about once per second, during the GPS
//  quiet time.  It's the best place to do anything that might take
//  a while: print a bunch of things, write to SD, send an SMS, etc.
//
//  By doing the "hard" work during the quiet time, the CPU can get back to
//  reading the GPS chars as they come in, so that no chars are lost.

static void doSomeWork()
{
  
  // Old SIM update stuff
  //  updateSerial();
  //  // Check that you’re connected to the network.
  //  simSerial.println("AT+COPS?");
  //  updateSerial();
  //  // Check that you’re registered on the network.
  //  // The second # should be 1 or 5. 1 indicates you are registered to home network and 5 indicates roaming network.
  //  // Other than these two numbers indicate you are not registered to any network.
  //  simSerial.println("AT+CREG?");
  //  // Check the ‘signal strength’ – the first # is dB strength.
  //  // It should be higher than around 5. Higher is better. Of course it depends on your antenna and location!
  //  simSerial.println("AT+CSQ");
  //  updateSerial();
  //  // Will return the lipo battery state.
  //  // The second number is the % full (in this case its 93%) 
  //  // and the third number is the actual voltage in mV (in this case, 3.877 V)
  //  simSerial.println("AT+CBC");
  //  updateSerial();

  // New sim stuff
  uint8_t  chargeState = -99;
  int8_t   percent     = -99;
  uint16_t milliVolts  = -9999;
  modem.getBattStats(chargeState, percent, milliVolts);
  DBG("Battery charge state:", chargeState);
  DBG("Battery charge 'percent':", percent);
  DBG("Battery voltage:", milliVolts / 1000.0F);
  
  // Print all the things!
  //  trace_all( DEBUG_PORT, gps, fix );

  // Data model description:
  // https://github.com/SlashDevin/NeoGPS/blob/master/extras/doc/Data%20Model.md

  display.clearDisplay();
  display.setCursor(0, 10);
  display.print("GPS status: ");
  display.print(fix.status);
  
  display.setCursor(0, 20);
//  display.print(fix.latitude());
  display.print(fix.latitudeDMS.degrees);  // https://github.com/SlashDevin/NeoGPS/issues/9
  display.print("d ");
  display.print(fix.latitudeDMS.minutes);  // https://github.com/SlashDevin/NeoGPS/issues/9
  display.print("' ");
  display.print(fix.latitudeDMS.secondsF());  // https://github.com/SlashDevin/NeoGPS/issues/9
  display.print("'' ");
  display.print(fix.latitudeDMS.NS());
  
  display.setCursor(0, 30);
//  display.print(fix.longitude());
  display.print(fix.longitudeDMS.degrees);  // https://github.com/SlashDevin/NeoGPS/issues/9
  display.print("d ");
  display.print(fix.longitudeDMS.minutes);  // https://github.com/SlashDevin/NeoGPS/issues/9
  display.print("' ");
  display.print(fix.longitudeDMS.secondsF());  // https://github.com/SlashDevin/NeoGPS/issues/9
  display.print("'' ");
  display.print(fix.longitudeDMS.EW());

  display.setCursor(0,40);
  display.print("Alt:");
  display.print(fix.alt.whole);
  display.print(" Kph:");
  display.print(fix.speed_kph()); 

  display.setCursor(0,50);
  if(fix.dateTime.hours < 10){
    display.print("0");
    display.print(fix.dateTime.hours);
  } else {
    display.print(fix.dateTime.hours);
  }
    
  display.print(":");
  if(fix.dateTime.minutes < 10){
    display.print("0");
    display.print(fix.dateTime.minutes);
  } else {
    display.print(fix.dateTime.minutes);
  }
  
  display.print(" ");
  display.print(fix.dateTime.date);
  display.print("/");
  display.print(fix.dateTime.month);

  // Battery millivolts
  display.print(" ");
  display.print(milliVolts);
  display.print("mV");
  
  
  display.display();



} // doSomeWork


// Time Zone stuff


void adjustTime( NeoGPS::time_t & dt )
{
  NeoGPS::clock_t seconds = dt; // convert date/time structure to seconds

  #ifdef CALCULATE_DST
    //  Calculate DST changeover times once per reset and year!
    static NeoGPS::time_t  changeover;
    static NeoGPS::clock_t springForward, fallBack;

    if ((springForward == 0) || (changeover.year != dt.year)) {

      //  Calculate the spring changeover time (seconds)
      changeover.year    = dt.year;
      changeover.month   = springMonth;
      changeover.date    = springDate;
      changeover.hours   = springHour;
      changeover.minutes = 0;
      changeover.seconds = 0;
      changeover.set_day();
      // Step back to a Sunday, if day != SUNDAY
      changeover.date -= (changeover.day - NeoGPS::time_t::SUNDAY);
      springForward = (NeoGPS::clock_t) changeover;

      //  Calculate the fall changeover time (seconds)
      changeover.month   = fallMonth;
      changeover.date    = fallDate;
      changeover.hours   = fallHour - 1; // to account for the "apparent" DST +1
      changeover.set_day();
      // Step back to a Sunday, if day != SUNDAY
      changeover.date -= (changeover.day - NeoGPS::time_t::SUNDAY);
      fallBack = (NeoGPS::clock_t) changeover;
    }
  #endif

  //  First, offset from UTC to the local timezone
  seconds += zone_offset;

  #ifdef CALCULATE_DST
    //  Then add an hour if DST is in effect
    if ((springForward <= seconds) && (seconds < fallBack))
      seconds += NeoGPS::SECONDS_PER_HOUR;
  #endif

  dt = seconds; // convert seconds back to a date/time structure

} // adjustTime


//------------------------------------
//  This is the main GPS parsing loop.

static void GPSloop()
{
  while (gps.available( gpsPort )) {

    // Read GPS data
    fix = gps.read();

    // Time zone stuff
    if (fix.valid.time && fix.valid.date) {
      adjustTime( fix.dateTime );

      DEBUG_PORT << fix.dateTime;
    }
    DEBUG_PORT.println();

    // Display shit
    doSomeWork();
  }

} // GPSloop



//------------------------------------
//  This is the main SIM serial parsing loop.

//void updateSerial()
//{
//  delay(500);
//  while (Serial.available()) 
//  {
//    simSerial.write(Serial.read());//Forward what Serial received to Software Serial Port
//  }
//  while(simSerial.available()) 
//  {
//    Serial.write(simSerial.read());//Forward what Software Serial received to Serial Port
//  }
//}
