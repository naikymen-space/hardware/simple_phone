# Microcontroller alternatives

## Arduino Nano

Podría usar un Nano, pero me tienta mucho el WiFi en los ESP.

## ESP8226 (NodeMCU 12E)

Ahora estoy usando este porque es más angosto. Entra en el protoboard y en el PCB que tengo.

## ESP32

Este ESP32 es demasiado ancho...

## RP Pico

No probé, pero parece prometedor! El pico tiene reloj interno "preciso" y eso viene bien.

* https://github.com/mcauser/micropython-waveshare-epaper
  * https://www.waveshare.com/wiki/Raspberry_Pi_Pico
