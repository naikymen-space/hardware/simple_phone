# Arduino libraries

Some potentially useful Arduino libraries.

TO-DO: list the libraries that are actually used in the project. 

## Menu

Casi todas son para displays LCD/TFT/etc.

* (no docs) AceMenu: https://www.arduino.cc/reference/en/libraries/acemenu/
* (no docs) arduino-menusystem: https://www.arduino.cc/reference/en/libraries/arduino-menusystem/
* https://www.arduino.cc/reference/en/libraries/gem/
* https://www.arduino.cc/reference/en/libraries/arduinomenu-library/
* ArduinoMenu library:
  * https://www.arduino.cc/reference/en/libraries/arduinomenu-library/
  * https://github.com/neu-rah/ArduinoMenu

Ejemplos:

* https://create.arduino.cc/projecthub/ronfrtek/arduino-oled-display-menu-with-option-to-select-e85f04
* https://www.electronics-lab.com/nokia-5110-lcd-based-arduino-datalogger-menu/
* https://duino-projects.com/menu-on-nokia-5110-lcd-display-with-arduino/
* https://create.arduino.cc/projecthub/danhostler1985/six-button-menu-configuration-70825e
* https://create.arduino.cc/projecthub/ianabcumming/a-simple-arduino-menu-with-an-lcd-254080
* https://www.theverge.com/2021/1/13/22229985/customizable-epaper-smartwatch-kit-arduino
  * Es un smartwatch basado en ESP32... OMG! https://watchy.sqfmi.com/
  * https://www.youtube.com/watch?v=TbeXV9sXUnI
  * https://watchy.sqfmi.com/docs/hardware

### Fron scratch

Abstractions?

1. "Apps launcher" at the top.
2. Apps: phone, agenda, sms, ...
3. Menus and entries:
  - Nested menus
  - Navigation: up, down, pageup, pgdown, ok, cancel/back
  - Input fields
