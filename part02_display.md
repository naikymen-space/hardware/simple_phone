# Display

Acá están mis pruebas con un display de E-paper (SPI) y con un OLED de 0.96 pulgadas (I2C).

## OLED display

Es un display que se conecta por I2C al ESP32.

Libraries:

```
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1
#define SCREEN_ADDRESS 0x3C
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
```

Para encontrar la dirección I2C del módulo usé un sketch "I2CScanner". Hay libraries pero es más simple copiar y subir el sketch de algún lado:

* https://create.arduino.cc/projecthub/abdularbi17/how-to-scan-i2c-address-in-arduino-eaadda
* https://github.com/todbot/arduino-i2c-scanner
* https://www.arduino.cc/reference/en/libraries/i2cscanner/


## Waveshare 1.54' SPI E-paper display

Lo unico que funciono fue usar los examples de las libraries. En particular, la library `GxEPD2` (ver codigo más abajo).

* NO ANDUVO: https://www.youtube.com/watch?v=OPaCF-XJhqc
  * NO ANDUVO: http://davidjwatts.com/youtube/waveShare154-wifiManager_clean.zip
* NO ANDUVO: https://www.instructables.com/Getting-Started-With-E-Paper-Display-Modules/
* NO ANDUVO: https://www.instructables.com/E-Paper-Display-Partial-Refresh-With-Arduino/
* https://www.instructables.com/Epaper-and-Arduino-UNO/
* https://www.instructables.com/E-paper-Display-With-ArduinoESP8266/

En retrospectiva, debería haber conseguido uno que use I2C.

### ESP32 examples

* https://www.instructables.com/ESP32-E-Paper-Thermometer/
* https://github.com/ZinggJM/GxEPD2/blob/master/examples/GxEPD2_WS_ESP32_Driver/GxEPD2_WS_ESP32_Driver.ino

### Conexiones SPI

El display es "SPI" y la documentación al respecto es poca/rara/erronea.

* Muy util:
  * https://en.wikipedia.org/wiki/Serial_Peripheral_Interface#Interface
  * https://github.com/ZinggJM/GxEPD2/blob/master/ConnectingHardware.md
* "DC pin": https://arduino.stackexchange.com/a/47746 (al final)
* ESP32:
  * https://github.com/ZinggJM/GxEPD2/blob/master/examples/GxEPD2_WS_ESP32_Driver/GxEPD2_WS_ESP32_Driver.ino
  * https://www.instructables.com/ESP32-SIM800L-and-Barrier-Sensor/

### Arduino nano

> `SPI: 10 (SS), 11 (MOSI), 12 (MISO), 13 (SCK).`
>
> These pins support SPI communication, which, although provided by the underlying hardware, is not currently included in the Arduino language.

* https://stackoverflow.com/a/29372932

### ESP32

> NOTE: this board uses "unusual" SPI pins and requires re-mapping of HW SPI to these pins in SPIClass
> this example shows how this can be done easily.

* https://github.com/ZinggJM/GxEPD2/blob/master/examples/GxEPD2_WS_ESP32_Driver/GxEPD2_WS_ESP32_Driver.ino


## E-paper display Libraries

No hay muchos ejemplos con el Arduino Nano.

### GxEPD

No pude hacerla andar.

* https://github.com/ZinggJM/GxEPD

Lineas importantes:

```
// Wiring para el Nano
// BUSY -> 7, RST -> 9, DC -> 8, CS-> 10, CLK -> 13, DIN -> 11
#include <GxGDEH0154D67/GxGDEH0154D67.h>  // 1.54" b/w
```

### GxEPD2

Esta anduvo! yay :D

* https://github.com/ZinggJM/GxEPD2
* https://learn.adafruit.com/adafruit-gfx-graphics-library/graphics-primitives
* http://adafruit.github.io/Adafruit-GFX-Library/html/functions_func.html

#### Arduino Uno/Nano configuration

Borré estas lineas:

```
// select the display constructor line in one of the following files (old style):
#include "GxEPD2_display_selection.h"
#include "GxEPD2_display_selection_added.h"
//#include "GxEPD2_display_selection_more.h" // private

// or select the display class and display driver class in the following file (new style):
#include "GxEPD2_display_selection_new_style.h"
```

Agregué estas lineas (ver `GxEPD2_HelloWorld.ino`):

```
// alternately you can copy the constructor from GxEPD2_display_selection.h or GxEPD2_display_selection_added.h to here
// e.g. for Arduino Nano:
#define MAX_DISPLAY_BUFFER_SIZE 800
#define MAX_HEIGHT(EPD) (EPD::HEIGHT <= MAX_DISPLAY_BUFFER_SIZE / (EPD::WIDTH / 8) ? EPD::HEIGHT : MAX_DISPLAY_BUFFER_SIZE / (EPD::WIDTH / 8))
GxEPD2_BW<GxEPD2_154_D67, MAX_HEIGHT(GxEPD2_154_D67)> display(GxEPD2_154_D67(/*CS=*/ SS, /*DC=*/ 8, /*RST=*/ 9, /*BUSY=*/ 7)); // GDEH0154D67
```

En uno de los archivos del sketch está el wiring sugerido del SPI:

```
// mapping suggestion for AVR, UNO, NANO etc.
// BUSY -> 7, RST -> 9, DC -> 8, CS-> 10, CLK -> 13, DIN -> 11
```

#### ESP32 configuration

Ver `code/GxEPD2_WS_ESP32_Driver/GxEPD2_WS_ESP32_Driver.ino`

Conexiones que probé:

```
// mapping of Waveshare ESP32 Driver Board
// BUSY -> 25, RST -> 26, DC -> 27,
// CS -> 15, CLK -> 13, DIN -> 14
```

Dado que en elsetup esté esto:

```
void setup()
{
  Serial.begin(115200);

  display.init(115200); // uses standard SPI pins, e.g. SCK(18), MISO(19), MOSI(23), SS(5)

  SPI.end();            // release standard SPI pins, e.g. SCK(18), MISO(19), MOSI(23), SS(5)

  SPI.begin(13, 12, 14, 15); // map and init SPI pins SCK(13), MISO(12), MOSI(14), SS(15)

  ...
```

> Nota: el pin 12 "MISO" no está conectado. Ese pin del SPI serviría para recibir datos del "slave" (de los periféricos), pero el display no devuelve datos.
