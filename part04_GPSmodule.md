
# GPS module

Modelo: `GY-NEO6MV2`

Se encuentra en internet también como "Ublox Neo6m GPS module".

## Conexiones

TX/RX normal, por SoftwareSerial.

En el ESP32 usando `NeoGPS` (ver más abajo):

* El pin `27` (G27) va al pin `Tx` del módulo GPS.
* El pin `26` (G27) va al pin `Rx` del módulo GPS.

## Libraries

Libraries para usar en el Arduino IDE.

### NeoGPS

Probé varias libraries y la que pude hacer andar mejor fue `NeoGPS`.

Hay que configurar el serial de la library, mi `GPSport.h` para el ESP32 quedó así:

```
#ifndef GPSport_h
#define GPSport_h

// varios comments...

#include <SoftwareSerial.h>
SoftwareSerial gpsPort(27,26);
#define GPS_PORT_NAME "SoftwareSerial(27,26)"
#define DEBUG_PORT Serial

#endif
```

Conexiones correspondientes con el ESP32:

* El pin `27` (G27) va al pin `Tx` del módulo GPS.
* El pin `26` (G26) va al pin `Rx` del módulo GPS.

Además, habilité estas opciones en `GPSfix_cfg.h`:

```
#define GPS_FIX_DATE
#define GPS_FIX_TIME
#define GPS_FIX_LOCATION
#define GPS_FIX_LOCATION_DMS
#define GPS_FIX_ALTITUDE
#define GPS_FIX_SPEED
#define GPS_FIX_HEADING
#define GPS_FIX_SATELLITES

// El resto de las opciones quedó comentado.
```

Para que reporte la hora en mi zona horaria, usé este código: [NMEAtimezone.ino](https://github.com/SlashDevin/NeoGPS/blob/master/examples/NMEAtimezone/NMEAtimezone.ino).

La configuración relevante a la zona horaria es:

```
static const int32_t          zone_hours   = -3L; // Argentina https://en.wikipedia.org/wiki/Coordinated_Universal_Time#/media/File:World_Time_Zones_Map.png
static const int32_t          zone_minutes =  0L; // usually zero
static const NeoGPS::clock_t  zone_offset  =
                                zone_hours   * NeoGPS::SECONDS_PER_HOUR +
                                zone_minutes * NeoGPS::SECONDS_PER_MINUTE;

static const uint8_t fallMonth =  3;
static const uint8_t fallDate  = 14; // latest 2nd Sunday
static const uint8_t fallHour  =  2;
static const uint8_t springMonth   = 11;
static const uint8_t springDate    =  7; // latest 1st Sunday
static const uint8_t springHour    =  2;
#define CALCULATE_DST
```
