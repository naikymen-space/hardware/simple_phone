EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L esp32breakout_library:ESP32-32_38PIN U?
U 1 1 6106A224
P 3150 2350
F 0 "U?" H 3150 3200 50  0000 C CNN
F 1 "ESP32-32_38PIN" H 3150 3300 50  0000 C CNN
F 2 "" H 3150 3250 50  0001 C CNN
F 3 "https://www.studiopieters.nl/esp32-pinout/" H 3150 3250 50  0001 C CNN
	1    3150 2350
	1    0    0    -1  
$EndComp
$Comp
L epaper_display:1.54_epaper_display U?
U 1 1 6106C5F0
P 3100 3800
F 0 "U?" H 3528 3551 50  0000 L CNN
F 1 "1.54_epaper_display" H 3528 3460 50  0000 L CNN
F 2 "" H 3100 3800 50  0001 C CNN
F 3 "" H 3100 3800 50  0001 C CNN
	1    3100 3800
	1    0    0    -1  
$EndComp
Text GLabel 3450 4150 3    50   Input ~ 0
3.3v
Text GLabel 2100 1500 0    50   Input ~ 0
3.3v
Text GLabel 3350 4150 3    50   Input ~ 0
GND
Text GLabel 4100 2100 2    50   Input ~ 0
GND
Text GLabel 2750 4150 3    50   Input ~ 0
G25
Text GLabel 2850 4150 3    50   Input ~ 0
G26
Text GLabel 2950 4150 3    50   Input ~ 0
G27
Text GLabel 4100 3000 2    50   Input ~ 0
G15
Text GLabel 2100 2600 0    50   Input ~ 0
G14
Text GLabel 2100 2900 0    50   Input ~ 0
G13
Text GLabel 2100 2300 0    50   Input ~ 0
G25
Text GLabel 2100 2400 0    50   Input ~ 0
G26
Text GLabel 2100 2500 0    50   Input ~ 0
G27
Text GLabel 3050 4150 3    50   Input ~ 0
G15
Text GLabel 3150 4150 3    50   Input ~ 0
G13
Text GLabel 3250 4150 3    50   Input ~ 0
G14
$EndSCHEMATC
